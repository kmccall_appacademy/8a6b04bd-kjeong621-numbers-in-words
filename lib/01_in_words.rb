class Fixnum

  def in_words
    return "zero" if self.zero?
    single = {'1' => "one", '2' => "two", '3' => "three",
      '4' => "four", '5' => "five", '6' => "six",
      '7' => "seven", '8' => "eight", '9' => "nine"}
    teens = {'10' => "ten", '11' => "eleven",
            '12' => "twelve", '13' => "thirteen", '14' => "fourteen",
            '15' => "fifteen", '16' => "sixteen", '17' => "seventeen",
            '18' => "eighteen", '19' => "nineteen"}
    tens = {'2' => "twenty", '3' => "thirty", '4' => "forty", '5' => "fifty",
            '6' => "sixty", '7' => "seventy", '8' => "eighty", '9' => "ninety"}
    place = {0=> '', 1 => " thousand", 2 => " million",
            3 => " billion", 4 => " trillion"}

    num = self
    split_with_three = [] #splitting integeter with lengths of 3 from the right

    until num.zero?
      split_with_three << num.to_s.rjust(3,'0')[-3..-1]
      num = num / 1000
    end

    result = []
    split_with_three.each do |el|
      temp = []
      temp << single[el[0]] + ' hundred' unless el[0].to_i.zero?

      if el[1] == '1'
        temp << teens[el[1..2]]
      else
        temp << tens[el[1]] unless el[1].to_i.zero?
        temp << single[el[2]] unless el[2].to_i.zero?
      end
      result << temp.join(' ')
    end

    number_in_words = []
    (0...result.length).each do |idx|
      number_in_words << (result[idx] << place[idx]) unless result[idx] == ""
    end
    number_in_words.reverse.join(" ")
  end
end
